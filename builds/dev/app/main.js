(function () {
	'use strict';
	
	var catalogApp = angular.module('catalogApp',['ngRoute']);
	catalogApp.controller('catalogCtrl',function($scope){
		$scope.viewClass = "col-md-12";
		$scope.phones = [
		{'name':'Apple iPhone 5S 16Gb','os':'7','screen':'5',
		'resolution':'1136x640','memory':'16 Гб, без слота для карт памяти',
		'communications':'3G, 4G LTE, Wi-Fi, Bluetooth, GPS, ГЛОНАСС','price':'20000',
		'img':'5s_white.jpg'},
		{'name':'Apple iPhone 6 16Gb','iOS':'7','screen':'6',
		'resolution':'1136x640','memory':'16 Гб, без слота для карт памяти',
		'communications':'3G, 4G LTE, Wi-Fi, Bluetooth, GPS, ГЛОНАСС','price':'35000',
		'img':'6s_black.jpg'},
		{'name':'Apple iPhone 6S 32Gb','iOS':'7','screen':'6',
		'resolution':'1136x640','memory':'32 Гб, без слота для карт памяти',
		'communications':'3G, 4G LTE, Wi-Fi, Bluetooth, GPS, ГЛОНАСС','price':'45000',
		'img':'6s_white.jpg'}
		];

		$scope.changeView = function(mode){
			if (mode == "list") {
				$scope.viewClass = "col-md-12";
			} else if (mode == "grid") {
				$scope.viewClass = "col-md-4";
			}
		}
	});
	catalogApp.config(['$routeProvider',function($routeProvider){
		$routeProvider.
      when('/home', {
        templateUrl: 'app/templates/home.html',
        controller: 'catalogCtrl'
      }).
      when('/about', {
        templateUrl: 'app/templates/about.html',
        controller: 'catalogCtrl'
      }).
      when('/delivery', {
        templateUrl: 'app/templates/delivery.html',
        controller: 'catalogCtrl'
      }).
      otherwise({
        redirectTo: '/home'
      });
	}]);

})();